import unittest
import os

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


class ShopSampleTests(unittest.TestCase):
    def setUp(self):
        if os.name == 'nt':
            self.driver = webdriver.Chrome(executable_path=r"C:\chromedriver\chromedriver.exe")
        else:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        self.base_url = 'https://demobank.jaktestowac.pl/logowanie_etap_1.html'

    def tearDown(self):
        print('tearDown!')

    def test_page_title(self):
        self.driver.get(self.base_url)
        expected_title = 'Demobank - Bankowość Internetowa - Logowanie'
        actual_title = self.driver.title
        with open("results.txt",'w+',encoding = 'utf-8') as file:
            file.write('Comparing {0}={1}'.format(expected_title, actual_title))
        self.assertEqual(expected_title, actual_title)

    def test_login_element(self):
        element_id = "login_id"
        self.driver.get(self.base_url)
        discount_banner = self.driver.find_element_by_id(element_id)
        self.assertIsNotNone(discount_banner)


if __name__ == '__main__':
    unittest.main()
